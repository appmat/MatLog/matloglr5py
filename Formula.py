import re
import collections


def parse(s: str):
    print(s)
    return Parse(s).reduce().reduce()  # formula()


# ==========================================================================================
#               Нижеследующий код был написан Апарневым А. Н.                              #
#               Разрешается использовать исключительно в образовательных целях.            #
#               Автор не несёт ответственности за использование кода третьими лицами.      #
# ==========================================================================================


# <formula> ::= 1 | 0 | <atom> |
#   (<formula>) |
#   <formula> | <formula> |
#   <formula> & <formula> |
#   <formula> > <formula> |
#   !<formula>
# <atom> ::= <letter>[<letter>|<digit>]*


def Parse(ln: str):
    # parsing
    Token = collections.namedtuple('Token', ['type', 'value', 'pos'])

    def tokenize(line):
        keywords = {'1', '0'}
        token_specification = [
            ('NEG', r'\!'),
            ('CONJ', r'\&'),
            ('DISJ', r'\|'),
            ('IMPL', r'\>'),
            ('OPSCOBE', r'\('),
            ('CLSCOBE', r'\)'),
            ('NAME', r'[A-Za-z][A-Za-z0-9]*'),
            ('TRUE', '1'),
            ('FALSE', '0'),
            ('MISMATCH', r'.')
        ]
        tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
        line_num = 1
        line_start = 0
        for mo in re.finditer(tok_regex, line):
            kind = mo.lastgroup
            value = mo.group()
            column = mo.start() - line_start

            if kind == 'MISMATCH':
                raise RuntimeError('{value!r} unexpected on line {line_num}')

            yield Token(kind, value, column)

    tokens = list(tokenize(ln))

    postfix = []
    opstack = []
    priors = {
        'OPSCOBE': 5,
        'NEG': 4,
        'CONJ': 3,
        'DISJ': 2,
        'IMPL': 1,
        'BIIMPL': 0}
    for i, token in enumerate(tokens):
        if token[0] in ['NAME', 'TRUE', 'FALSE']:
            token = Token('NAME', token[1], token[2])
            postfix.append(token)
        elif token[0] in ['NEG', 'CONJ', 'DISJ', 'IMPL', 'BIIMPL']:
            while len(opstack) > 0 and opstack[-1][0] != 'OPSCOBE' and (
                    priors[opstack[-1][0]] > priors[token[0]] or
                    priors[opstack[-1][0]] == priors[token[0]] and
                    opstack[-1][0] in ['CONJ', 'DISJ', 'IMPL', 'BIIMPL']):
                postfix.append(opstack.pop())
            opstack.append(token)
        elif token[0] == 'OPSCOBE':
            opstack.append(token)
        elif token[0] == 'CLSCOBE':
            k = 0
            while len(opstack) > 0 and opstack[-1][0] != 'OPSCOBE':
                postfix.append(opstack.pop())
                k = k + 1
            if opstack[-1][0] == 'OPSCOBE':
                opstack.pop()
            else:
                raise RuntimeError('Scobe error')

    while len(opstack) > 0:
        postfix.append(opstack.pop())

    stack = []
    while len(postfix) > 0:
        token = postfix.pop(0)
        if token[1] == '1':
            stack.append(ConFormula())
        elif token[1] == '0':
            stack.append(DisFormula())
        elif token[0] == 'NAME':
            stack.append(AtomFormula(token[1]))
        elif token[0] == 'NEG':
            stack.append(NegFormula(stack.pop()))
        elif token[0] == 'CONJ':
            l = [stack.pop(), stack.pop()]
            l.reverse()
            stack.append(ConFormula(l))
        elif token[0] == 'DISJ':
            l = [stack.pop(), stack.pop()]
            l.reverse()
            stack.append(DisFormula(l))
        elif token[0] == 'IMPL':
            l = [stack.pop(), stack.pop()]
            l.reverse()
            stack.append(DisFormula([NegFormula(l[0]), l[1]]))
        elif token[0] == 'BIIMPL':
            l = [stack.pop(), stack.pop()]
            l.reverse()
            stack.append(DisFormula([ConFormula([l[0], l[1]]), ConFormula([NegFormula(l[0]), NegFormula(l[1])])]))
    return stack.pop()


class Formula:
    def __init__(self, members=[]):
        self.members = members

    def __str__(self):
        return self.__repr__()

    def lit(self):
        return None

    def reduce(self):
        return self

    def __hash__(self):
        return self.__repr__().__hash__()

    def __eq__(self, other):
        return hash(self) == hash(other) or str(self) == str(other) or str(self.reduce()) == str(other.reduce())


class AtomFormula(Formula):
    def __init__(self, name):
        self.name = name
        self.members = None

    framed = False

    def copy(self):
        return AtomFormula(self.name)

    def __repr__(self, br=False):
        if self.framed:
            m = '[{}]'
        else:
            m = '{}'
        return m.format(str(self.name))


class NegFormula(Formula):
    def __init__(self, value):
        self.value = value

    def reduce(self):
        t = type(self.value)
        if t is AtomFormula:
            return self
        elif t is NegFormula:
            return self.value.value.reduce()
        else:
            m = [NegFormula(memb).reduce() for memb in self.value.members]
            if t is ConFormula:
                r = DisFormula(m)
            else:
                r = ConFormula(m)
            # print(self,'2',r.reduce())
            return r.reduce()

    def lit(self):
        return self.value.lit()

    def __repr__(self, br=False):
        return ('!{}' if type(self.value) in [AtomFormula, NegFormula] else '!({})').format(str(self.value))


class SetFormula(Formula):
    def lit(self):
        return set.union(*[a.lit() for a in self.members])


class ConFormula(SetFormula):
    def __init__(self, members=[]):
        m = []
        for mm in members:
            if type(mm) is ConFormula and len(mm.members) > 0:
                m.extend(mm.members)
            else:
                m.append(mm)
        self.members = m.copy()

    def reduce(self):
        r = [memb.reduce() for memb in self.members]
        r1 = []
        for t in r:
            if str(t) == '0':
                return DisFormula([])
            if str(t) != '1':
                r1.append(t)
        return ConFormula(r1)

    def __repr__(self, br=False):
        return '1' if len(self.members) == 0 else ('({})' if br else '{}').format(
            '&'.join([a.__repr__(br=(type(a) == DisFormula)) for a in self.members]))


class DisFormula(SetFormula):
    def __init__(self, members=[]):
        m = []
        for mm in members:
            if type(mm) is DisFormula and len(mm.members) > 0:
                m.extend(mm.members)
            else:
                m.append(mm)
        self.members = m.copy()

    def reduce(self):
        # return DisForm([memb.reduce() for memb in self.members])

        r = [memb.reduce() for memb in self.members]
        r1 = []
        for t in r:
            if str(t) == '1':
                return ConFormula([])
            if str(t) != '0':
                r1.append(t)
        return DisFormula(r1)

    def __repr__(self, br=False):
        return '0' if len(self.members) == 0 else ('({})' if br else '{}').format(
            '|'.join([str(a) for a in self.members]))


strs = ["A|B", "A&B", "A>B", "C&(A|B)", "!C&!(A>!B|1)"]

for s in strs:
    print(s)
    f = Parse(s)
    print(f)
    f = f.reduce()
    print(f)
    f = f.reduce()
    print(f)
    print('\n' * 2)
