import sys
import PyQt5.QtWidgets as qw
from PyQt5 import uic
from Formula import parse

class MainForm(qw.QMainWindow):
    def __init__(self):
        super(MainForm, self).__init__()
        uic.loadUi('MainForm.ui', self)

        self.pushButton=self.findChild(qw.QPushButton,'pushButton')
        self.pushButton.clicked.connect(self.pushButtonClicked)

        self.textEdit=self.findChild(qw.QTextEdit,'textEdit')

    def pushButtonClicked(self):
        s=self.textEdit.toPlainText()
        self.formula=parse(s)
        self.textEdit.setPlainText(str(self.formula))
