import sys
import PyQt5.QtWidgets as qw
from MainForm import MainForm

if __name__ == '__main__':
    app = qw.QApplication(sys.argv)

    # w = QWidget()
    # w.resize(250, 150)
    # w.move(300, 300)
    # w.setWindowTitle('Simple')

    w = MainForm()
    w.show()
    sys.exit(app.exec_())
